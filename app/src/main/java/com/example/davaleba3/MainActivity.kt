package com.example.davaleba3

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var btn1 : Button
    private lateinit var btn2 : Button
    private lateinit var btn3 : Button
    private lateinit var btn4 : Button
    private lateinit var btn5 : Button
    private lateinit var btn6 : Button
    private lateinit var btn7 : Button
    private lateinit var btn8 : Button
    private lateinit var btn9 : Button

    private var activePlayer : Int = 1
    private var firstPlayer = ArrayList<Int>()
    private var secondPlayer = ArrayList<Int>()
    private var btnNumber : Int = 0
    private var winner : Int = 0
    private var defaultBtnColor = Color.parseColor("#FF3700B3")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init(){
        btn1 = findViewById(R.id.btn1)
        btn2 = findViewById(R.id.btn2)
        btn3 = findViewById(R.id.btn3)
        btn4 = findViewById(R.id.btn4)
        btn5 = findViewById(R.id.btn5)
        btn6 = findViewById(R.id.btn6)
        btn7 = findViewById(R.id.btn7)
        btn8 = findViewById(R.id.btn8)
        btn9 = findViewById(R.id.btn9)
    }

    fun updateGameProgress(view: View) {
        if (view is Button) {
            when(view.id) {
                R.id.btn1 -> btnNumber = 1
                R.id.btn2 -> btnNumber = 2
                R.id.btn3 -> btnNumber = 3
                R.id.btn4 -> btnNumber = 4
                R.id.btn5 -> btnNumber = 5
                R.id.btn6 -> btnNumber = 6
                R.id.btn7 -> btnNumber = 7
                R.id.btn8 -> btnNumber = 8
                R.id.btn9 -> btnNumber = 9
            }

            if(activePlayer == 1) {
                view.text = "X"
                firstPlayer.add(btnNumber)
                checkWinCombination()
                println(firstPlayer)
                activePlayer = 2
            }
            else {
                view.text = "O"
                secondPlayer.add(btnNumber)
                checkWinCombination()
                println(secondPlayer)
                activePlayer = 1
            }

            view.isEnabled = false
            view.setBackgroundColor(Color.MAGENTA)
        }
    }

    private fun checkWinCombination() {

        if (activePlayer == 1) {
            if(firstPlayer.contains(1) && firstPlayer.contains(2) && firstPlayer.contains(3)
                || firstPlayer.contains(4) && firstPlayer.contains(5) && firstPlayer.contains(6)
                || firstPlayer.contains(7) && firstPlayer.contains(8) && firstPlayer.contains(9)
                || firstPlayer.contains(1) && firstPlayer.contains(4) && firstPlayer.contains(7)
                || firstPlayer.contains(2) && firstPlayer.contains(5) && firstPlayer.contains(8)
                || firstPlayer.contains(3) && firstPlayer.contains(6) && firstPlayer.contains(9)
                || firstPlayer.contains(1) && firstPlayer.contains(5) && firstPlayer.contains(9)
                || firstPlayer.contains(3) && firstPlayer.contains(5) && firstPlayer.contains(7))
            {
                winner = 1
                checkWinner()
            }

        } else {
            if(secondPlayer.contains(1) && secondPlayer.contains(2) && secondPlayer.contains(3)
                || secondPlayer.contains(4) && secondPlayer.contains(5) && secondPlayer.contains(6)
                || secondPlayer.contains(7) && secondPlayer.contains(8) && secondPlayer.contains(9)
                || secondPlayer.contains(1) && secondPlayer.contains(4) && secondPlayer.contains(7)
                || secondPlayer.contains(2) && secondPlayer.contains(5) && secondPlayer.contains(8)
                || secondPlayer.contains(3) && secondPlayer.contains(6) && secondPlayer.contains(9)
                || secondPlayer.contains(1) && secondPlayer.contains(5) && secondPlayer.contains(9)
                || secondPlayer.contains(3) && secondPlayer.contains(5) && secondPlayer.contains(7))
            {
                winner = 2
                checkWinner()
            }
        }
    }

    private fun checkWinner() {
        if(winner == 1) {
            Toast.makeText(this, "\"X\" won", Toast.LENGTH_SHORT).show()
        } else if(winner == 2) {
            Toast.makeText(this, "\"O\" won", Toast.LENGTH_SHORT).show()
        }

        if(btn1.text == "") {
            btn1.isEnabled = false
            btn1.setBackgroundColor(defaultBtnColor)
        }
        if(btn2.text == "") {
            btn2.isEnabled = false
            btn2.setBackgroundColor(defaultBtnColor)
        }
        if(btn3.text == "") {
            btn3.isEnabled = false
            btn3.setBackgroundColor(defaultBtnColor)
        }
        if(btn4.text == "") {
            btn4.isEnabled = false
            btn4.setBackgroundColor(defaultBtnColor)
        }
        if(btn5.text == "") {
            btn5.isEnabled = false
            btn5.setBackgroundColor(defaultBtnColor)
        }
        if(btn6.text == "") {
            btn6.isEnabled = false
            btn6.setBackgroundColor(defaultBtnColor)
        }
        if(btn7.text == "") {
            btn7.isEnabled = false
            btn7.setBackgroundColor(defaultBtnColor)
        }
        if(btn8.text == "") {
            btn8.isEnabled = false
            btn8.setBackgroundColor(defaultBtnColor)
        }
        if(btn9.text == "") {
            btn9.isEnabled = false
            btn9.setBackgroundColor(defaultBtnColor)
        }
    }

    fun restartGame(view: View) {
        if (view is Button) {
            activePlayer = 1
            firstPlayer.clear()
            secondPlayer.clear()

            btn1.isEnabled = true
            btn2.isEnabled = true
            btn3.isEnabled = true
            btn4.isEnabled = true
            btn5.isEnabled = true
            btn6.isEnabled = true
            btn7.isEnabled = true
            btn8.isEnabled = true
            btn9.isEnabled = true

            btn1.text = ""
            btn2.text = ""
            btn3.text = ""
            btn4.text = ""
            btn5.text = ""
            btn6.text = ""
            btn7.text = ""
            btn8.text = ""
            btn9.text = ""

            btn1.setBackgroundColor(defaultBtnColor)
            btn2.setBackgroundColor(defaultBtnColor)
            btn3.setBackgroundColor(defaultBtnColor)
            btn4.setBackgroundColor(defaultBtnColor)
            btn5.setBackgroundColor(defaultBtnColor)
            btn6.setBackgroundColor(defaultBtnColor)
            btn7.setBackgroundColor(defaultBtnColor)
            btn8.setBackgroundColor(defaultBtnColor)
            btn9.setBackgroundColor(defaultBtnColor)

        }
    }

}